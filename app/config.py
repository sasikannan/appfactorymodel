from os import path

basedir = path.abspath(path.dirname(__file__))


class Config(object):

    SQLALCHEMY_DATABASE_URI = "mysql+pymysql://root:12345@localhost/sasi"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ECHO = False
    TESTING = False
    CSRF_ENABLED = True
    DEBUG = True

