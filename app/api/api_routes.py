from flask import Blueprint,request
from app.services import Service

####### Blueprint for api routes #######

site= Blueprint(__name__, "site")

###### api routes url ###########

@site.route('/',methods=["GET"])
def home():
      return "<h1> WELCOME TO STUDENTS PROFILE <h1>"

@site.route("/employee/post",methods=["POST"])
def post():
      #requesting to get json format name and email value

      name = request.json["name"]
      email = request.json["email"]

      #calling a class from services and passing the values name,email json in it

      response = Service.add_employee(name,email)

      # got data from services class method

      return response

@site.route("/employee/getall",methods=["GET"])
def getall():
      # calling a class from services to get all values

      response = Service.getall_employee()

      # got data from services class method

      return response

@site.route("/employee/get/<id>",methods=["GET"])
def get(id):
      # calling a class from services and passing the id from url which is needed to get

      response = Service.get_employee(id)

      # got data from services class method

      return response

@site.route("/employee/put/<id>",methods=["PUT"])
def put(id):
      # requesting to get json format name and email value to change or update the data

      name = request.json["name"]
      email = request.json["email"]

      # calling a class from services and passing the values name,email json in it to update or change values

      response = Service.put_employee(id,name,email)

      #got data from services class method

      return response

@site.route("/employee/delete/<id>",methods=["DELETE"])
def delete(id):
      # calling a class from services and passing the id from url which is needed to delete

      response = Service.del_employee(id)

      # got data from services class method

      return response










