from app.model import employee


class Service():

    #passing values name and email from routes
    @classmethod
    def add_employee(cls,name,email):

        #calling a classmethod from students table and passing the values name and email

        new_employee= employee.post_emp(name,email)

        #after returning and get dict data from students table classmethod here stu returns the data and passing data to routes response

        return new_employee

    # passing request here to getall
    @classmethod
    def getall_employee(cls):
        # calling a classmethod from students table

        getall_employees = employee.getall_emp()

        # after returning and get dict data from students table classmethod here stu returns the data and passing data to routes response

        return getall_employees

    # passing request id here to get values
    @classmethod
    def get_employee(cls, id):

        # calling a classmethod and passing id from students table

        getby_employeeid = employee.get_emp(id)

        # after returning and get dict data from students table classmethod here stu returns the data and passing data to routes response

        return getby_employeeid

    # passing values name and email from routes
    @classmethod
    def put_employee(cls,id,name,email):

        # calling a classmethod and passing id,values from students table

        update_employee = employee.put_emp(id,name,email)

        #after returning and get dict data from students table classmethod here stu returns the data and passing data to routes response

        return update_employee

    @classmethod
    def del_employee(cls,id):

        # calling a classmethod and passing id from students table

        delete_employee = employee.del_emp(id)

        # after returning and get dict data from students table classmethod here stu returns the data and passing data to routes response

        return delete_employee
