import unittest
from app.services import Service
from app import create_app
from main import app


class EmployeeTest(unittest.TestCase):
    app = create_app()
    app.app_context().push()

    def test_post_students(self):
            result = Service.add_employee(name="ramkara", email="ramskuaru@gmail.com")
            self.assertEqual("lrkkravi@gmail.com", result.get("email"))

    def test_getbyid(self):
            result= Service.get_employee(8)
            self.assertEqual("lrkkravi@gmail.com",result.get("email"))


    def test_getall(self):
            result = Service.getall_employee()
            self.assertEqual(not None, result is not None)

    def test_put(self):
            result = Service.put_employee(id=14, name="rasi", email="rkrasasasai@gmail.com")
            self.assertEqual("rkrasasasai@gmail.com", result.get("email"))

    def test_del(self):
            result = Service.del_employee(14)
            self.assertEqual("Employee data Deleted", result)


if __name__=="__main__":
    unittest.main()



