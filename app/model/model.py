from app import db
# from app.model import Base



class employee(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80),unique=True)
    email=db.Column(db.String(80),unique=True)


    def __init__(self,name,email):
        self.name=name
        self.email=email


    @classmethod
    def post_emp(cls, name, email):
        # assigning passing value as variable

        employee1 = employee(name=name, email=email)

        # calling class from base it save and commit the data

        from app.model import Base
        emp_save = Base.save_to_db_employee(employee1)

        # calling class from base it convert object to dict and return value as dict

        return Base.Sqlalchemyobj_todict(emp_save)

    # got values from services and performing get process in database
    @classmethod
    def getall_emp(cls):
        # calling class from base querying all from database

        from app.model import Base
        emp_getall = Base.get_allemployee()

        # calling class from base it convert object to dict and return value as dict
        return Base.obj_todict_getall(emp_getall)

    # got values from services and performing get process in database
    @classmethod
    def get_emp(cls, id):
        # calling class from base query by id from database

        from app.model import Base
        emp_getid = Base.get_employeebyid(id)

        # calling class from base it convert object to dict and return value as dict

        return Base.obj_todict(emp_getid)

    # got values from services and performing put process in database
    @classmethod
    def put_emp(cls, id, name, email):
        # get id from database to update

        from app.model import Base
        emp_getid = Base.get_employeebyid(id)

        # assigning passing data as variable to database

        employee1 = employee(name=name, email=email)

        # performing update

        emp_getid.name = name
        emp_getid.email = email

        # calling class from base and commit the data

        Base.update()

        # calling class from base it convert object to dict and return value as dict

        return Base.Sqlalchemyobj_todict(emp_getid)

    # got values from services and performing delete process in database
    @classmethod
    def del_emp(cls, id):
        # get id from database to delete

        from app.model import Base
        employee1 = Base.get_employeebyid(id)

        # calling class from base and delete the id

        return Base.delete_from_db_employee(employee1)



        # calling class from base it convert object to dict and return value as dict


