from app import db
from app.model import employee


class Base():
    #converting object to dict

    def obj_todict(self):
        emp_dict = self.__dict__
        emp_dict.pop('_sa_instance_state')
        return emp_dict

    #converting sqlalchemyobj to dict

    def Sqlalchemyobj_todict(self):
        result = {
            "id": self.id,
            "name": self.name,
            "email": self.email
        }
        return result

    #iterate converted object to get all data

    def obj_todict_getall(self):
        emp = {"employees": []}
        for i in self:
            emp_dict = Base.obj_todict(i)
            emp["employees"].append(emp_dict)
        return emp

    #get students query from database by id

    def get_employeebyid(id):
         employees1 = employee.query.get(id)
         return employees1

    # getall students query from database by id

    def get_allemployee():
        employees1 = employee.query.all()
        return employees1

    #commit query to update

    def update():
        db.session.commit()

    #add and commit query

    def save_to_db_employee(self):
        db.session.add(self)
        db.session.commit()
        return self

    #delete and commit query

    def delete_from_db_employee(self):
        db.session.delete(self)
        db.session.commit()
        return "Employee data Deleted"










