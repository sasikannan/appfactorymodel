from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from app.config import Config


db = SQLAlchemy()

def create_app(config_class=Config):
    app = Flask(__name__, instance_relative_config=False)
    app.config.from_object(config_class)
    db.init_app(app)

    with app.app_context():
        from app.api import site
        app.register_blueprint(site)
        db.create_all()
        return app
